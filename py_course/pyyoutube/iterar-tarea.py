#Iterar una lista de 0 a 10 solo los numeros divisibles 3
for i in range (10):
   if i%3 ==0:
        print(i)

# Forma distinta de realizar el mismo ejercicio.
x = range(11)
for num in x:
    if num % 3 == 0:
        print(num)