# Imprimir solo las letras a

for letra in "Holanda":
    if letra == "a":
        print(letra)
        break  #Rompe el ciclo for cuando encuentra la primer letra a
    

# Imprimir solo numeros pares

#for i in range (6)
    #if i%2 == 0:
     #   print(i)

# Imprimir solo numero pares
for i in range (6):
    if i%2 != 0: # Un valor distinta de "0" va a volver a realizar el ciclo de comparacion debido al continue.
        continue
    print(i)

for e in range (10):
    if e%2 == 0:
        print(e)