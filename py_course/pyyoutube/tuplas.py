# Tupla mantien el orden, pero ya no se puede modificar

frutas = ("Naranjas", "Manzanas", "Peras", "Bananas")
print(frutas)
# largo de la tupla 
print(len(frutas))
#acceder al indice
print (frutas [0])
# navegacion inverda 
print (frutas [-1])
# rango
print (frutas [0:2])

# convertir una tupla en lista y luego volver a convertir la lista a tupla
frutasListas = list(frutas)
frutasListas [1] = "mandarinas"
frutas = tuple(frutasListas)
print (frutas) 
#iterar una tupla
for fruta in frutas:
    print(fruta)
#Si queremos evitar el salto de linea y agregar un espacion entre cada elemento
# solo agregamos al print (end= " ")
for fruta in frutas:
    print(fruta,end=" ")

for fruta in frutas:
    print(fruta,end=", ")
#No podemos agregar ni elimar elementos de una tupla
#del frutas
#print(frutas)

