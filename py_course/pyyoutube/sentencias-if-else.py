condicion = True
if condicion == True:
    print("La condicion es verdadera")
elif condicion == False:
   print("La condicion es falsa")
else:
    print("La condicion no reconocida")
# Forma de realizar una evaluacion verdadero o falso, solo recomendado
#cuando la evaluacion del codigo es pequeña.
print("Condicion Verdadera") if condicion else print ("Condicion Falsa")
    
numero = int(input("Proporcionar un numero entre 1 y 3: "))
if numero == 1:
    numeroTexto = "numero uno"
elif numero == 2:
    numeroTexto = "numero 2"
elif numero == 3:
    numeroTexto = "numero 3"
else: 
    numeroTexto = "Fuera de rango"
    
print("Numero Proporcionado:", numeroTexto)