# Los operadores logicos nos permiten comparar dos valores buleanos#
a = 3
valorMinimo = 0
valorMaximo = 5
dentroRango = (a >= valorMinimo and a <= valorMaximo)
#print(dentroRango)
if(dentroRango):
    print("Dentro de Rango")
else:
    print("Fuera de rango")
    
vacaciones = True
diaDescanso = True
if (vacaciones or diaDescanso):
    print("Pudes ir al parque")
else:
    print("Tines deberes que hacer")
    
print(not(vacaciones))

#(Not) Cambia el resultado de mi logica