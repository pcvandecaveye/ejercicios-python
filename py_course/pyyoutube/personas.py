#Clases: Las clases tiene atributos y metodos 
class Persona: 
    #pass #Pasar
    def __init__(self, nombre, edad): # Definir la clase con atributos
        self.nombre = nombre #Atributos
        self.edad = edad
print(Persona)

#Modificar los valores

Persona.nombre = "Juan"
Persona.edad = 28

#Acceder a los valores
print(Persona.nombre)
print(Persona.edad)

# La clase en si misma es un Objeto. 

#creacion de un nuevo objeto a partir de una clase

persona = Persona ("Karla", 30) 
print(persona.nombre)
print(persona.edad)
#creacion de otro objeto

persona2 = Persona ("carlos", 40)
print(persona2.nombre)
print(persona2.edad)