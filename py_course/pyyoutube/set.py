# set es una coleccion sin orden y tampoco tiene indices, no permite elementos repetidos 
#los elementos no se pueden modificar, pero si agregar nuesvos o eliminar

planetas = {"Marte","Jupiter", "Venus"}
print(planetas)

# revisar el largo 
print(len(planetas))
#revisar si un elemento esta presente
print("Marte" in planetas)
#agregar
planetas.add("Tierra")
print(planetas)

planetas.add ("Tierra") # no se pueden agregar elementos duplicados
print(planetas)

# eliminar con remove posiblemente arroja excepcion  
planetas.remove("Tierra")
print(planetas)
#eliminar con discard no arroja excepcion
planetas.discard("Venus")
print(planetas)
#limpiar set
planetas.clear()
print(planetas)
#elimiar set
#del planetas
#print(planetas)

