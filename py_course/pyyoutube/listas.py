#nombres = ["Juan", "Karla", "Ricador", "Maria"]
#print(nombres)

# Cada elemento (Nombre) esta representado por un indie
# Juan = Elemento 1==> Indice 0
# Karla = Elemento 2==> Indice 1
# Ricardo = Elemento 3==> Indice 2
# Maria = Elemento 4==> Indice 3

nombres = ["Juan", "Karla", "Ricador", "Maria"]
print(nombres)
# Conocer el largo de la lista (cantidad de elementos)
print(len(nombres))
#elemento 0
print(nombres[0])
print(nombres[1])
#Navegacion inversa (nos trae los valores inversa de la lista )
print(nombres[-1])
print(nombres [-2])
# Recuperar un rango 
print(nombres[0:2])# Sin incluir el indice 2
#Imprimir los elementos de inicio hasta el indice proporcionado
print(nombres[:3])#sin incluir el inde 3
#imprimir los elementos hasta el final desdesde el indice proporcionado
print(nombres[1:])
# cambiar los elementos de una lista
nombres [3]="Ivone"
print(nombres)
#interar la lista
for nombre in nombres:
    print(nombre)
#revisar si un elemento existe en la lista
d = str(input("Colocar un nombre:"))
if d in nombres:
    print("Karla si existe")
else:
    print("El elemento buscado no existe en la lista")
# Agregar un nuevo elemento a la lista
nombres.append ("Lorenzo")
print(nombres)

# insertar un nuevo elemento en el indice proporcionado
nombres.insert(1,"Roberto")
print(nombres)
#remover un elemento
nombres.remove("Roberto")
print(nombres)
#remover el ultimo elemento de nuesta lista
nombres.pop()
print(nombres)
#remover el indice indicado de la lista
del nombres [0]
print(nombres)
#Limpiar todos los elementos de la lista
nombres.clear()
print(nombres)
#eliminar por completo la lista
del nombres
print(nombres)