# Atributos mas metodos

class Aritmetica:
    """Clase aritmentica para realizar operaciones de sumar,restar etc"""
    def __init__(self, operando1, operando2): #El nombre del Parametro No tiene que ser igual al nombre del atributo, pero es lo comun.
        self.operando1 = operando1
        self.operando2 = operando2
        
    def sumar(self):
        """Se realiza la operacion con los atributos de la clase"""
        return self.operando1 + self.operando2
    def restar(self):
        return self.operando1 - self.operando2
    def multiplicar(self):
        return self.operando1 * self.operando2
    def dividir(self):
        return self.operando1 / self.operando2
    
    """""Creamos un nuevo objeto"""
    
aritemetica = Aritmetica(2,4)
print("Resultado de la suma: ", aritemetica.sumar())
print("Resultado de la resta: ", aritemetica.restar())
print("Resultado de la Multiplicacion:", aritemetica.multiplicar())
print ("Resultado de la Division:", aritemetica.dividir())