# Trabajar con diccionarios, un diccionarios esta compuesto por llave, valor

diccionario = {
    "IDE":"Integrated Development Environment",
    "OOP": "Objet Oriented Programing",
    "DBMS": "Database Management System"
}

print(diccionario)
#larogo de un diccionario
print(len(diccionario))
#accediendo a un elemento
print(diccionario["IDE"])
#Otra forma de acceder a los elementos y obterner el mismo resultado
print(diccionario.get("IDE"))
#Modificar valores
diccionario["IDE"]="INTEGRTED DEVELOMNET ENVIRONMENT"
print(diccionario)
#interar 
for termino in diccionario:
    print(termino)
# valor asociadas a cada llave 
for termino in diccionario:
    print(diccionario[termino])
# Obtenenos los valores pero no de las llaves asociadas
for valor in diccionario.values():
    print(valor)
    
#comprar si un elemento existe 
print("IDE" in diccionario)
#agregar nuevos elementos
diccionario["PK"]= "Primary Key"
print(diccionario)
#remover elementos
diccionario.pop("DBMS")
print(diccionario)

#limpiar diccionario
diccionario.clear()
print(diccionario)
#eliminar por completo
#del diccionario
#print(diccionario)