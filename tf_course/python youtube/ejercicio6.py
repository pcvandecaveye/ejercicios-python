num1 = int(input("Intruducir primer valor: "))
num2 = int(input("Introducir segundo valor: "))
num3 = int(input("Introducir tercer valor: "))

# Funcion anidada donde se imprimer el mayor
if num1 > num2 and num1 > num3:
    print(num1)
else:
    if num2 > num3:
        print(num2)
    else:
        print(num3)

# Funcion anidada donde se imprime el menor
if num1 < num2 and num1 < num3:
    print(num1)
else:
    if num2 < num3:
        print(num2)
    else:
        print(num3)
