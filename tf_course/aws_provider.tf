
provider "aws" {
  region                  = "us-east-1"
  shared_credentials_file = "C:/Users/pcvan/.aws//credentials"
  profile                 = "user-tf-aws"
}
resource "aws_instance" "Test" {
  ami           = "ami-0947d2ba12ee1ff75"
  instance_type = "t2.micro"
}
#Creacion VPC

resource "aws_vpc" "main" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    "Name" = "lab_tf"
  }
}
resource "aws_subnet" "main" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "lab_tf_subnet01"
  }
}
